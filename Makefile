
TARGET= entretien.pdf fiche.pdf contributions.pdf

all: $(TARGET)

%.pdf: %.tex
	pdflatex $<

.PHONY: clean mrproper

mrproper: clean
	rm -f $(TARGET)

clean:
	rm -f $(TARGET:%.pdf=%.aux)
	rm -f $(TARGET:%.pdf=%.log)
